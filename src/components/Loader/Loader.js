import React from 'react';
import Spinner from '../../loader.gif';
import './Loader.css';

function Loader(props) {
  if (props.isLoaded === false) {
    return (
      <div className="Loader">
        <img src={Spinner} alt={Spinner} />
      </div>
    );
  } else return null;
}
export default Loader;
