import axios from 'axios';
import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from '../components/Header/Header';
import Stories from '../components/Stories/Stories';
import Loader from '../components/Loader/Loader';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stories: [],
      isLoaded: false,
    };
  }

  componentDidMount() {
    axios
      .get('https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty')
      .then(storyIds => {
        return Promise.all(
          storyIds.data.slice(0, 30).map(story => this.getStory(story))
        );
      })
      .then(topStories => {
        this.setState({
          stories: topStories,
          isLoaded: true,
        });
      });
  }

  fetchStories = type => {
    this.setState({
      isLoaded: false,
    });
    axios
      .get(`https://hacker-news.firebaseio.com/v0/${type}.json`)
      .then(storyIds => {
        return Promise.all(
          storyIds.data.slice(0, 30).map(storyId => this.getStory(storyId))
        );
      })
      .then(selectedStory => {
        this.setState({
          stories: selectedStory,
          isLoaded: true,
        });
      });
  };

  getStory = async id => {
    try {
      const story = await axios.get(
        `https://hacker-news.firebaseio.com/v0/item/${id}.json`
      );
      return story;
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <Router>
        <div className="App">
          <Loader isLoaded={this.state.isLoaded} />
          <Header fetchStories={this.fetchStories} />
          <Stories stories={this.state.stories} />
        </div>
      </Router>
    );
  }
}

export default App;
