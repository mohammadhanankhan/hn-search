import React from 'react';
import { NavLink } from 'react-router-dom';
import Logo from '../../logo.png';
import './Header.css';

const Header = props => {
  return (
    <>
      <div className="Header-logo">
        <div>
          <img src={Logo} alt={Logo} />
        </div>
        <h1>
          Search <span> Hacker News</span>
        </h1>
      </div>
      <div className="Header-link">
        <NavLink
          onClick={() => props.fetchStories('topstories')}
          to="/top"
          activeClassName="active"
        >
          Top Stories
        </NavLink>
        <NavLink
          to="/news"
          activeClassName="active"
          onClick={() => props.fetchStories('newstories')}
        >
          New Stories
        </NavLink>
        <NavLink
          to="/best"
          activeClassName="active"
          onClick={() => props.fetchStories('beststories')}
        >
          Best Stories
        </NavLink>
      </div>
    </>
  );
};

export default Header;
