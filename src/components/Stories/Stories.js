import React from 'react';
import './Stories.css';

const Link = ({ url, title }) => (
  <a href={url} target="_blank" rel="noreferrer">
    {title}
  </a>
);

const Stories = ({ stories }) => {
  const story = stories.map(story => {
    const { id, by, title, kids, time, url } = story.data;
    return (
      <div className="Story">
        <div className="Story-title">
          <Link url={url} title={title} />
        </div>
        <div className="Story-info">
          <span>
            by{' '}
            <Link
              url={`https://news.ycombinator.com/user?id=${by}`}
              title={by}
            />
          </span>
          |
          <span>
            {new Date(time * 1000).toLocaleDateString('en-US', {
              hour: 'numeric',
              minute: 'numeric',
            })}
          </span>
          |
          <span>
            <Link
              url={`https://news.ycombinator.com/item?id=${id}`}
              title={`${kids && kids.length > 0 ? kids.length : 0} comments`}
            />
          </span>
        </div>
      </div>
    );
  });

  return <div>{story}</div>;
};

export default Stories;
